<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/** local settings - so we can properly share the wp-config.php */
if (file_exists(dirname(__FILE__) . '/wp-config-local.php')) {
  # IMPORTANT: ensure your local config does not include wp-settings.php
  require_once(dirname(__FILE__) . '/wp-config-local.php');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'r*`+!p7%S)~.T2+xYr&EQWKi%<*%~WXPyE,m`5x{S0uj6/aQ+m`!B=w?I2qL`5?d');
define('SECURE_AUTH_KEY',  'BMxU>3V0@!vHwz[>.7oH6T;OrO<|jp4wH]H}ew.$B-VZ`0x^lojB6A.A}j)]!hj0');
define('LOGGED_IN_KEY',    '#|wi!X Q?-M-7_G($f~8R=xkpw9@nQ<7{?mLA?<LfjesED?0]mGI2cFWg6{^n*;2');
define('NONCE_KEY',        'K/44TviX8`R;QW]|Tg]N+E)pMR9.WnfjiJ|aPCU|?_wJc,(teTA,-C|)|m+(j1w5');
define('AUTH_SALT',        'TEUz3Rb(mAe0xk`gnun5%>Qut/ODC,=3|7NASKn/xa2A$na1Yn=?/0aV$B2 W8?i');
define('SECURE_AUTH_SALT', '75+$,OZAWO5McEJ+LD& 2r&L61l*h}nvRHeR?Ix/?$PEpuoUPpBT1H`_MTJzbi]{');
define('LOGGED_IN_SALT',   ')*R_Q*mL+b-c&_fKuM.[w]!Xy:O;wEpd_$O16+9pOiU.w%CqZcEosh5#->uvzuu;');
define('NONCE_SALT',       'I/rb@$>GQXqw`vzo}FF|I1O<2+Z}11j,,1_le-J|m@<e %[Y8p4*Mp87P{*Oq.&~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpdb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
