
var $j = jQuery.noConflict();

$j(document).ready(function() {
	"use strict";

	$j(window).on("load", function() {
	var zoom = false;
	$j(".vc_col-sm-12").each(function(){
		if ($j(this).hasClass("zoooom")) zoom = true;
	});

	if (zoom) {
		$j(".header_bottom a, .footer_top a").click(function(){
$j(".zoomContainer").remove();
		});
	}});
$j(".icon_holder").click(function(){
	var href;
	href = $j(this).parent().find('a').attr('href');
	window.location.href = href;
});

$j(".zoooom").click(function(){
	if ($j( window ).width() < 7768) {
		var href;
		href = $j(this).find('img').attr('src');
		window.location.href = href;
	}
});});
